//
//  Ease.swift
//  GTween
//
//  Created by Goon Nguyen on 10/10/14.
//  Copyright (c) 2014 Goon Nguyen. All rights reserved.
//

import Foundation

struct Ease {
    static var Linear:String    = "Linear"
    static var Back:String      = "Back"
    static var Elastic:String   = "Elastic"
    static var Quint:String     = "Quint"
    static var Sine:String      = "Sine"
    static var Bounce:String    = "Bounce"
    static var Expo:String      = "Expo"
    static var Circ:String      = "Circ"
    static var Cubic:String     = "Cubic"
    static var Quad:String      = "Quad"
    static var Quart:String     = "Quart"
}